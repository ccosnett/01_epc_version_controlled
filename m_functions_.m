d = Import["./00_data/measurements_.mx"];

fingersUniversal[{im_, x1_, x2_, x3_} /; (Abs[Abs[x1 - x2] - Abs[x2 - x3]] < 50)] := Module[
   {x, y, offset, fingerL, fingerR},
   {x, y} = ImageDimensions[im];
   offset = 0;
   fingerL = ImageTrim[im, {{x1 (x/x3), 1}, {x2 (x/x3), y}}];
   fingerR = ImageTrim[im, {{x2 (x/x3), 1}, {x3 (x/x3), y}}];
   {fingerL, fingerR}
							 ];


trimmer[im_] := Module[{x, y}, {x, y} = ImageDimensions[im];
   ImageTrim[im, ({{2/3 x, 1.2/10 y}, {1 x, 8.3/10 y}})]];



singleIntegersToDouble[lis_] := Module[{value, conf}, value = StringJoin[ToString /@ (First /@ lis)];
						      conf = Round[#,0.001]&@ Mean[Last /@ lis];
   {value, conf}];




pipeline[im_] := 
 Module[{output0, output1, output2, output3, output4, output5, 
   output6, lines, leftXs, rightXs, Xs, sortedXs, output7, output8, 
   output9, output10, output11, output12, output13, output14, 
   output15, output16, output17},
  output0 = ImageCrop@im;
  output1 = ImageResize[#, {500, 500}] &@output0;
  output2 = trimmer@output1;
  output3 = ColorNegate@output2;
  output4 = ColorConvert[#, "Grayscale"] &@output3;
  output5 = GaussianFilter[#, 1, {0, 1}] &@output4;
  output6 = Binarize[#, 0.05] &@output5;
  lines = ImageLines[#, Method -> "RANSAC"] &@output6;
  leftXs = First /@ First @@@ lines;
  rightXs = First /@ Last @@@ lines;
  Xs = (leftXs + rightXs)/2;
  sortedXs = Sort[Xs];
  (*SQL insert will make sure there are 3 of em*)
  (*they seem to be \
mostly right*)
  output7 = {output2}~Join~sortedXs;
  output8 = fingersUniversal[output7];
  output9 = ImagePad[#, -5] & /@ output8;
  output10 = ImageCrop /@ output9;
  output11 = 
   ColorReplace[#, {_ -> Black, White -> White}] & /@ output10;
  output12 = DeleteBorderComponents /@ output11;
  output13 = ImageCrop /@ output12;
  output14 = ColorNegate /@ output13;
  output15 = ImagePad[#, 14, White] & /@ output14;
  output16 = 
   TextRecognize[#, "Character", {"Text", "Strength"}, 
      RecognitionPrior -> "Word"] & /@ output15;
  output17 = singleIntegersToDouble /@ output16
  ];
