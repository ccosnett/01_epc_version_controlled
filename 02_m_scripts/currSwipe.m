#!/usr/bin/env wolframscript


Needs["DatabaseLink`"];
conn = OpenSQLConnection[
  JDBC["MySQL(Connector/J)", "localhost:3306/ocr?useSSL=false"], 
  "Username" -> "student", "Password" -> "1.2323951EsC"];
SetDirectory[NotebookDirectory[]];

db = DatabaseReference[<|"Backend" -> "MySQL", "Host" -> "localhost", 
   "Port" -> 3306, "Name" -> "ocr", "Username" -> "student", 
   "Password" -> "1.2323951EsC"|>];


dat = ExternalEvaluate[db, "SELECT * FROM png_exists_x_y"];
dl = List @@@ Normal@dat[All, {"id", "f1", "x", "y"}];




currentXQ[{a_, {x_, y_}}] := Module[{te},
   te = TextRecognize[a];
   (*Print[te];*)
   If[HammingDistance[StringPadRight[te , 7], "Current"] < 3, x, 
    Nothing]
   ];
potentialXQ[{a_, {x_, y_}}] := Module[{te},
   te = TextRecognize[a];
   If[HammingDistance[StringPadRight[te , 9], "Potential"] < 3, x, 
    Nothing]
   ];

PotentialX[lis_] := Module[{li, len},
   li = Cases[potentialXQ /@ lis, _?NumberQ ];
   len = Length[li];
   If[len == 0,
    {0, 0},
    If[len == 1,
     {Delete[li, 0], 0}, {Delete[li, 0]}]
    ]
    ];



CurrentX[lis_] := Module[{li, len},
   li = Cases[currentXQ /@ lis, _?NumberQ ];
   len = Length[li];
   If[len == 0,
    {0, 0},
    If[len == 1,
     {Delete[li, 0], 0}, {Delete[li, 0]}]
    ]
    ];




fun[{id_, path_, x_, y_}] := Module[{Nothing
    , curr, pot
    , im
    , ore
    , cx
    , px
    , out
    , rectangle1X1
    , rectangle1X2
    , rectangle2X1
    , rectangle2X2
    , rectangle3X1
    , rectangle3X2
    , rectangle4X1
    , rectangle4X2
    , rectangle1Y1
    , rectangle1Y2
    , rectangle2Y1
    , rectangle2Y2
    
				    },
				    
   im = Import[path, IncludeMetaInformation -> None];
   im = ImageResize[im , Scaled[3]];
   ore = FindImageText[im, "Word", {"Image", "RegionCentroid"}];
   url= "https://www.rightmove.co.uk/properties/"<>ToString[id];
   cx = CurrentX@ore;
   px = PotentialX@ore;
   {cx[[1]], px[[1]]};
   {xN, yN} = ImageDimensions[im];
   rectangle1X1 = cx[[1]] - Abs[px[[1]] - cx[[1]]]/2;
   rectangle1Y1 = 0 + rectangle1X1*1/6.5;
   rectangle1X2 = cx[[1]] + Abs[px[[1]] - cx[[1]]]/2;
   rectangle1Y2 = yN - 2 rectangle1X1*1/6;
   rectangle2X1 = px[[1]] - Abs[px[[1]] - cx[[1]]]/2;
   rectangle2Y1 = 0 + rectangle1X1*1/6.5;
   rectangle2X2 = px[[1]] + Abs[px[[1]] - cx[[1]]]/2;
   rectangle2Y2 = yN - 2 rectangle1X1*1/6;
   
   trawl1 = Cases[ore 
     , {a_, 
       b_} /;
      ((rectangle1X1 < b[[1]] < 
          rectangle1X2) && (rectangle1Y1 < b[[2]] < rectangle1Y2))
     ];
   trawl2 = Cases[
     ore , {a_, 
       b_} /; ((rectangle2X1 < b[[1]] < 
          rectangle2X2) && (rectangle2Y1 < b[[2]] < rectangle2Y2))
     ];
   curr = If[
     Length[trawl1] == 1,
     TextRecognize[Binarize@ColorNegate[trawl1 /. {{a_, b_}} :> a] , 
      RecognitionPrior -> "Character"],  
     0];
   pot = If[
     Length[trawl2] == 1,
     TextRecognize[Binarize@ColorNegate[trawl2 /. {{a_, b_}} :> a ]  ,
       RecognitionPrior -> "Character"] ,  
     0];
   out = {id,url, curr, pot, path, xN, yN, Delete[cx , 0], Delete[px , 0],rectangle1X1, rectangle1X2, rectangle2X1, rectangle2X2};
   (* Print[out]; *)
   (* Print[out]; *)
   Print[out];
   out
   ];



Do[
    	
	Print[j];
	fj=fun[
		dl[[j]]
	   ];
	Print[fj];
	SQLInsert[conn, "png_CurrentX", {"id","url" ,"current", "potential", "f1", "x", "y", "current1X","current2X","potential1X","potential2X"
					,"rectangle1X1"
					,"rectangle1X2"
					,"rectangle2X1"
					,"rectangle2X2"},  fj],
	{j, 1, 140000, 1}]
