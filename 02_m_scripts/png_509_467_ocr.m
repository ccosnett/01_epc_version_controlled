#!/usr/bin/env wolframscript


Needs["DatabaseLink`"];
conn = OpenSQLConnection[
  JDBC["MySQL(Connector/J)", "localhost:3306/ocr?useSSL=false"], 
  "Username" -> "student", "Password" -> "1.2323951EsC"];
SetDirectory[NotebookDirectory[]];

db = DatabaseReference[<|"Backend" -> "MySQL", "Host" -> "localhost", 
   "Port" -> 3306, "Name" -> "ocr", "Username" -> "student", 
   "Password" -> "1.2323951EsC"|>];







name = "png_509_467_ocr";
(* SQLDropTable[conn, name]; *)
SQLCreateTable[conn, name , {
	                                   SQLColumn["id", "DataTypeName" -> "BIGINT"]
					 , SQLColumn["url", "DataTypeName" -> "TEXT"]
					 , SQLColumn["f1", "DataTypeName" -> "TEXT"]
					 , SQLColumn["x", "DataTypeName" -> "INTEGER"]
					 , SQLColumn["y", "DataTypeName" -> "INTEGER" ]
					 , SQLColumn["ocr_output_CURRENT", "DataTypeName" -> "NUMERIC" ]
					 , SQLColumn["ocr_confidence_CURRENT", "DataTypeName" -> "FLOAT" ]
					 , SQLColumn["ocr_output_POTENTIAL", "DataTypeName" -> "NUMERIC" ]
					 , SQLColumn["ocr_confidence_POTENTIAL", "DataTypeName" -> "FLOAT" ]
  
  },
 "Index" -> "id"]






dat = ExternalEvaluate[db, "SELECT * FROM png_exists_x_y WHERE x=509 AND y=467"];
dl = List @@@ Normal@dat[All, {"id", "f1", "x", "y"}];
Print["length dl = "<>ToString[Length[dl]]];

(*
thir[im_] := Module[{x, y, thi, whiteImage, finger},
   {x, y} = ImageDimensions[im];
   finger = {ImageTrim[im, ( {
        {4/6 x + 30, 1 + 70},
        {5/6 x - 15, y - 85}
       } )]}
   ];
 *)




thir[im_] := Module[{x, y, thi, whiteImage},
  {x, y} = ImageDimensions[im];
  {fingerL, fingerR} = {ImageTrim[im, ( {
       {4/6 x + 30, 1 + 70},
       {5/6 x - 15, y - 85}
      } )], ImageTrim[im, ( {
       {5/6 x + 23, 1 + 70},
       {6/6 x - 22, y - 85}
      } )]}
  ];



funky[lis_] := Module[{value, conf},
  value = StringJoin[ToString /@ (First /@ lis)];
  conf = Mean[Last /@ lis ];
  {value, conf}
  ];


fun[{id_, path_, x_, y_}] := Module[{},
				    imag = Import[path,IncludeMetaInformation->None];
				    url = "https://www.rightmove.co.uk/properties/"<>ToString[id];
				    rectangles = thir@imag;
				    squares = ImageCrop /@ rectangles;
				    preprocessed = Binarize/@ColorNegate/@squares;
				    out = TextRecognize[#, "Character", {"Text", "Strength"}, RecognitionPrior -> "Word"] &/@preprocessed;
				    out1 = funky /@ out;
				    {outL, outR} = out1;
				    out2 = {id, url, path, x, y, outL[[1]], Round[outL[[2]] , 0.0001], outR[[1]], Round[outR[[2]] , 0.0001]}];



columns=		  {
			  "id"
			  ,"url"
			 ,"f1"
			, "x"
			, "y"
			 ,"ocr_output_CURRENT"
			 ,"ocr_confidence_CURRENT"
			 ,"ocr_output_POTENTIAL"
			 ,"ocr_confidence_POTENTIAL"
		          };




Do[
    	
	Print[j];
	fj=fun[
		dl[[j]]
	   ];
	Print[fj];
	SQLInsert[conn, "png_509_467_ocr", columns,  fj],
	{j, 50813, 65000, 1}]
