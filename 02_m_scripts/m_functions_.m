d = Import["./00_data/measurements_.mx"];

randomImage[{x_, y_}] := Module[{subtable, im},
   subtable = d[Select[{#x, #y} == {x, y} &]];
   im = RandomChoice[Normal[subtable[All, "path"] ] ];
   Import[im]
   ];

randomImage[{x_, y_, ext_}] := Module[{subtable, im},
   subtable = d[Select[{#x, #y, #ext} == {x, y, ext} &]];
   im = RandomChoice[Normal[subtable[All, "path"] ] ];
   Import[im]
			       ];

randomImage[{x_, y_}, ext_] := Module[{subtable, im},
   subtable = d[Select[{#x, #y, #ext} == {x, y, ext} &]];
   im = RandomChoice[Normal[subtable[All, "path"] ] ];
   Import[im]
   ];


trimmer[im_] := Module[{x, y},
  {x, y} = ImageDimensions[im];
  ImageTrim[im, ( {
     {2/3 x, 1/10 y},
     {1 x, 8/10 y}
    } )]
  ];
