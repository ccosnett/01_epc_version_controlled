#!/usr/bin/env wolframscript

beg =272000;
end= 286000;

Print["paths imported \n\n"];
Print["beginning = "<>ToString[beg]];
Print["ending = "<>ToString[end]];

db = DatabaseReference[<|"Backend" -> "MySQL", "Host" -> "localhost", 
   "Port" -> 3306, "Name" -> "ocr", "Username" -> "student", 
   "Password" -> "1.2323951EsC"|>];

     
Needs["DatabaseLink`"];
conn = OpenSQLConnection[
  JDBC["MySQL(Connector/J)", "localhost:3306/ocr?useSSL=false"], 
  "Username" -> "student", "Password" -> "1.2323951EsC"];
SetDirectory[NotebookDirectory[]];


name = "measurements";
(*SQLDropTable[conn, name];*)
SQLCreateTable[conn, name , {
	SQLColumn["id", "DataTypeName" -> "BIGINT"]
					 , SQLColumn["url", "DataTypeName" -> "TEXT"]
					 , SQLColumn["path", "DataTypeName" -> "TEXT"]
					 , SQLColumn["ext", "DataTypeName" -> "TEXT"]
					 , SQLColumn["x", "DataTypeName" -> "INTEGER"]
					 , SQLColumn["y", "DataTypeName" -> "INTEGER" ]
					 , SQLColumn["fileSize", "DataTypeName" -> "FLOAT" ]
					 , SQLColumn["mean", "DataTypeName" -> "FLOAT" ]
					 , SQLColumn["standardDeviation", "DataTypeName" -> "FLOAT" ]
					 , SQLColumn["entropy", "DataTypeName" -> "FLOAT" ]
					 , SQLColumn["firstWord", "DataTypeName" -> "TEXT"]
					 , SQLColumn["secondWord", "DataTypeName" -> "TEXT"]
					 , SQLColumn["string", "DataTypeName" -> "TEXT"]
  
  },
 "Index" -> "id"]



	(*	   "id"
		  ,"url"
		  ,"path"
		  , "ext"
		  , "x"
		  , "y"
		  ,"fileSize"
		  ,"mean"
		  ,"standardDeviation"
		  ,"entropy"
		  ,"firstWord"
		  ,"secondWord"
		  ,"stringg"
	 *)





da = Import["./00_data/rsamp.mx"];




fun[row_] := Quiet@Module[
    {id, path, dims, im}
    
    , {id, path} = Last /@ Normal@Normal[da[row, All]];
    path = "./00_data" <> StringDrop[path, 1];
    im = Import[path, IncludeMetaInformation -> None];
    dims = ImageDimensions@im;
    filesize = QuantityMagnitude[FileSize[path]];
    word = TextRecognize[im, "Word"];
    firstWord = word[[1]];
    secondWord = word[[2]];
    mean = ImageMeasurements[im, "Mean"];
    standardDeviation = ImageMeasurements[im, "StandardDeviation"];
    entropy = ImageMeasurements[im, "Entropy"];
    stringg = If[
      StringContainsQ[path, ___ ~~ "-ee-" ~~ ___],
      "ee",
      If[StringContainsQ[path, ___ ~~ "-ei-" ~~ ___], "ei" , "un"] ];
    url = "https://www.rightmove.co.uk/properties/"<>ToString[id];
    {
	    id,
	    url
	  , path
	  , FileExtension[path]
	  , dims[[1]]
	  , dims[[2]]
	  , filesize
	  , Mean@mean
	  , Mean@standardDeviation
	  , entropy
	  , firstWord
	  , secondWord
	  , stringg}
    ];


(*Do[
	Print[j];
	fj=fun[j];
	Print[fj];
	SQLInsert[conn, "measurements", {
		   "id"
		  ,"url"
		  ,"path"
		  , "ext"
		  , "x"
		  , "y"
		  ,"fileSize"
		  ,"mean"
		  ,"standardDeviation"
		  ,"entropy"
		  ,"firstWord"
		  ,"secondWord"
		  ,"string"
		  },  fj],
	{j, 78148, 140000, 1}]

 *)


Print["here"];Print["here"];Print["here"];


Print[
AbsoluteTiming[
	Do[
	{id, path} = Last /@ Normal@Normal[da[i, All]];
   bool = (First@First@SQLExecute[conn, "SELECT COUNT(1) FROM measurements WHERE id = "<>ToString[id]<>";"]);
   Print[bool];
   Print[id];
   Print[i];
   columns =  {
		   "id"
		  ,"url"
		  ,"path"
		  , "ext"
		  , "x"
		  , "y"
		  ,"fileSize"
		  ,"mean"
		  ,"standardDeviation"
		  ,"entropy"
		  ,"firstWord"
		  ,"secondWord"
		  ,"string"
   };
   If[bool >= 1, (Print[StringRepeat["already done\n",2]]),
      
	     (j=i;
	fj=fun[j];
	Print[fj];
	Print[j];
	SQLInsert[conn, "measurements",columns,  fj])
   ]
   
   ,{i, end, beg,-1}
]
]
]
