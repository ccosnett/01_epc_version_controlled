#!/usr/bin/env wolframscript


Needs["DatabaseLink`"];
conn = OpenSQLConnection[
  JDBC["MySQL(Connector/J)", "localhost:3306/ocr?useSSL=false"], 
  "Username" -> "student", "Password" -> "1.2323951EsC"];
SetDirectory[NotebookDirectory[]];
da = Import["./00_data/png_exists.mx"];


fun[row_] := Quiet@Module[{id, path, dims},
   {id, path} = Last /@ Normal@Normal[da[row, All]];
   path = "./00_data" <> StringDrop[path , 1];
   dims = ImageDimensions@Import[path, IncludeMetaInformation->None];
   {id, path, dims[[1]], dims[[2]]}
   ]

Do[
	Print[j];
	fj=fun[j];
	Print[fj];
	SQLInsert[conn, "png_exists_x_y", {"id", "f1", "x", "y"},  fj],
	{j, 140000, 0, -1}]
