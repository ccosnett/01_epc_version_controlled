#!/usr/bin/env wolframscript


Needs["DatabaseLink`"];
conn = OpenSQLConnection[
  JDBC["MySQL(Connector/J)", "localhost:3306/ocr?useSSL=false"], 
  "Username" -> "student", "Password" -> "1.2323951EsC"];
SetDirectory[NotebookDirectory[]];

db = DatabaseReference[<|"Backend" -> "MySQL", "Host" -> "localhost", 
   "Port" -> 3306, "Name" -> "ocr", "Username" -> "student", 
   "Password" -> "1.2323951EsC"|>];


dat = ExternalEvaluate[db, "SELECT * FROM png_exists_x_y"];
dl = List @@@ Normal@dat[All, {"id", "f1", "x", "y"}];




CurrentX[lis_] := Module[{li, len},
   li = Cases[currentXQ /@ lis, _?NumberQ ];
   len = Length[li];
   If[len == 0,
    {0, 0},
    If[len == 1,
     {Delete[li, 0], 0}, {Delete[li, 0]}]
    ]
    ];
currentXQ[{a_, {x_, y_}}] := Module[{te},
   te = TextRecognize[a];
   (*Print[te];*)
   If[HammingDistance[StringPadRight[te , 7], "Current"] < 3, x, 
    Nothing]
   ];
PotentialX[lis_] := Module[{li, len},
   li = Cases[potentialXQ /@ lis, _?NumberQ ];
   len = Length[li];
   If[len == 0,
    {0, 0},
    If[len == 1,
     {Delete[li, 0], 0}, {Delete[li, 0]}]
    ]
    ];
potentialXQ[{a_, {x_, y_}}] := Module[{te},
   te = TextRecognize[a];
   If[HammingDistance[StringPadRight[te , 9], "Potential"] < 3, x, 
    Nothing]
   ];






fun[{id_, path_, x_, y_}] := Module[{ore, cx, px, out},
   ore = FindImageText[Import[path, IncludeMetaInformation -> None], 
     "Word", {"Image", "RegionCentroid"}];
   cx = CurrentX@ore;
   px = PotentialX@ore;
   out = {id, path, x, y, Delete[cx, 0], Delete[px, 0]}
   ];


Do[
	
	Print[j];
	fj=fun[
		dl[[j]]
	   ];
	Print[fj];
	SQLInsert[conn, "png_CurrentX", {"id", "f1", "x", "y", "currentX","potentialX"},  fj],
	{j, 1, 140000, 1}]
