#!/usr/bin/env wolframscript


	       (**)
$QUERY = "SELECT id,path,x,y FROM measurements 
WHERE 
   (x=27500 AND y=258) OR
   (x=33600 AND y=315) OR
   (x=509 AND y=467) OR
   (x=71900 AND y=676)

";

	       
Get["./m_functions_.m"];

Needs["DatabaseLink`"];

name = "parser0_output";

(* SQLDropTable[conn, name]; *)



conn = OpenSQLConnection[
  JDBC["MySQL(Connector/J)", "localhost:3306/ocr?useSSL=false"], 
  "Username" -> "student", "Password" -> "1.2323951EsC"];

SetDirectory[NotebookDirectory[]];

db = DatabaseReference[<|"Backend" -> "MySQL", "Host" -> "localhost", 
   "Port" -> 3306, "Name" -> "ocr", "Username" -> "student", 
   "Password" -> "1.2323951EsC"|>];






SQLCreateTable[conn, name , {
	                                   SQLColumn["id", "DataTypeName" -> "BIGINT"]
					 , SQLColumn["url", "DataTypeName" -> "TEXT"]
					 , SQLColumn["path", "DataTypeName" -> "TEXT"]

					 , SQLColumn["first_word_detected_on_image", "DataTypeName" -> "TEXT"]				      , SQLColumn["amount_of_redness", "DataTypeName" -> "TEXT"]
					 , SQLColumn["type_of_image", "DataTypeName" -> "TEXT"]
					 , SQLColumn["x", "DataTypeName" -> "INTEGER"]
					 , SQLColumn["y", "DataTypeName" -> "INTEGER" ]
					 , SQLColumn["ocr_output_CURRENT", "DataTypeName" -> "NUMERIC" ]
					 , SQLColumn["ocr_confidence_CURRENT", "DataTypeName" -> "FLOAT" ]
					 , SQLColumn["ocr_output_POTENTIAL", "DataTypeName" -> "NUMERIC" ]
					 , SQLColumn["ocr_confidence_POTENTIAL", "DataTypeName" -> "FLOAT" ]
  
  },
 "Index" -> "id"]





dat = ExternalEvaluate[db, $QUERY];
dl = List @@@ Normal@dat[All, {"id", "path", "x", "y"}];
Print["length dl = "<>ToString[Length[dl]]];

(*
thir[im_] := Module[{x, y, thi, whiteImage, finger},
   {x, y} = ImageDimensions[im];
   finger = {ImageTrim[im, ( {
        {4/6 x + 30, 1 + 70},
        {5/6 x - 15, y - 85}
       } )]}
   ];
 *)





typeWord[im_] := Module[{wor},
   wor = TextRecognize[ColorNegate@im, "Word" ][[1]];
   wor
   (*If[wor\[Equal]"Energy","ee",If[wor\[Equal]"Environmental","ei",
   "notsure"]]*)
   ];
typeColor[im_] := Module[{m, value},
   m = ImageMeasurements[#, "Total"] &;
			value = Round[m@ColorDetect[im, Red],1];
   value
   (*If[value > 5000, "ee",If[wor\[Equal]"Environmental","ei",
   "notsure"]]*)
   ];







fun[{id_, path_, x_, y_}] := Module[{},
				    imag = Import[path,IncludeMetaInformation->None];
				    url = "https://www.rightmove.co.uk/properties/"<>ToString[id];
				    amountRedness=typeColor[imag];
				    firstWord=typeWord[imag];
				    type=If[amountRedness <143,"EI","EE"];
				    out1 = pipeline[imag];
				    {outL, outR} = out1;
				    out2 = {
					    id
					  
					   , url
					   , path
					   , firstWord
					   ,amountRedness
					   ,type
					   , x
					  
					   ,y
					  , outL[[1]]
					  , outL[[2]]
					  , outR[[1]]
					  , outR[[2]]
				    }];



columns=		  {
			  "id"
			  ,"url"
			 ,"path"
			 ,"first_word_detected_on_image"
			 ,"amount_of_redness"
			  ,"type_of_image"
			, "x"
			, "y"
			 ,"ocr_output_CURRENT"
			 ,"ocr_confidence_CURRENT"
			 ,"ocr_output_POTENTIAL"
			 ,"ocr_confidence_POTENTIAL"
		          };




Do[
      	{id, path} = {dl[[j,1]],dl[[j,3]]};
	bool = (First@First@SQLExecute[conn, "SELECT COUNT(1) FROM "<>name<>" WHERE id = "<>ToString[id]<>";"]);
	Print[id];
	Print[bool];
	Print[j];
(*	SQLInsert[conn, "four_clusters_ocr", columns,  fj];*)

   If[bool >= 1, (Print[StringRepeat["already done\n",2]]),
      
      (fj=fun[
		dl[[j]]
	   ];
	Print[fj];

	(* fj=fun[j]; *)
	Print[fj];
	Print[fj];
	Print[fj];
	Print[fj];
	
	Print[j];
	Print[j];
	Print[j];
	Print[j];
	Print[j];
	SQLInsert[conn, name, columns,  fj]
	     )
   ]
   

      ,
	{j,  1,200000, 1}]
